(function() {
    'use strict';

    angular.module('app').service('contactDao', ContactDao);
    ContactDao.$inject = ["$http", "$log", "$q"];

    function ContactDao($http, $log, $q) {

        this.getContacts = getContacts;
        this.getContact = getContact;
        this.deleteContacts = deleteContacts;
        this.deleteContact = deleteContact;
        this.saveContact = saveContact;

        function getContacts() {
            return $http.get('api/contacts').then(function (result) {
                return result.data;
            }).catch();
        }

        function getContact(id) {
            return $http.get('api/contacts/' + id).then(function (result) {
                return result.data;
            }).catch(function (errorResponse) {
                return $log.error('Error: ' + errorResponse.data);
            })
        }

        function deleteContacts() {

        }

        function deleteContact(id) {
            $http.delete('api/contacts/' + id).catch(u);
        }

        function saveContact(obj)
        {
            return obj._id ? $http.put('api/contacts/' + obj._id, obj) :  $http.post("api/contacts", obj);

        }

        function u(result) {
            return $log.error('Error: ' + result.data), e.reject(result);
        }

    }

})();