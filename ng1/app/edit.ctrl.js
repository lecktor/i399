(function () {
    'use strict';

    angular.module('app').controller('EditCtrl', EditCtrl);

    function EditCtrl($routeParams, $location, contactDao) {

        var vm = this;
        this.submitForm = submitForm; // o
        this.errors = [];
        this.contact = {};

        init();

        function init() {
            if ($routeParams.id === undefined) {
                return;
            }
            contactDao.getContact($routeParams.id).then(function(result) {
                vm.contact = result;
            })
        }

        function submitForm() {
            contactDao.saveContact(vm.contact).then(function() {
                $location.path('/search');
            })
        }
    }

    EditCtrl.$inject = ["$routeParams", "$location", "contactDao"];

})();