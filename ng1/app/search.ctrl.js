(function () {
    'use strict';

    angular.module('app').controller('SearchCtrl', SearchCtrl);
    SearchCtrl.$inject = ["contactDao", "modalService"];

    function SearchCtrl(contactDao, modalService) {

        var vm = this;
        this.contacts = [];
        this.searchString = "";
        this.deleteContact = deleteContact; // c
        this.deleteSelected = deleteSelected; // o
        this.nameCodeFilter = nameCodeFilter; // r
        init();

        function init() {
            contactDao.getContacts().then(function(result) {
                vm.contacts = result;
            });
        }

        function deleteContact(contact) {
            modalService.confirm().then(function() {
                return contactDao.deleteContact(contact)
            }).then(init);
        }

        function deleteSelected() {
            var selected = this.contacts.filter(function (result) {
                return result.selected;
            }).map(function (result) {
                return result._id;
            });
            modalService.confirm().then(function () {
                return this.deleteContacts(selected)
            }).then(init());
        }

        function nameCodeFilter(contact) {
            return 0 === vm.searchString.length || filter(contact.name) || filter(contact.phone);
        }

        function filter(s) {
            return void 0 !== s && s.toLowerCase().indexOf(vm.searchString.toLowerCase()) >= 0
        }
    }

})();