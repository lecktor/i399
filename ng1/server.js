'use strict';

const express = require('express');
const app = express();
const Dao = require('./dao.js');
const bodyParser = require('body-parser');

app.post('/api/contacts', bodyParser.json(), createContact);
app.get('/api/contacts', getContacts);
app.get('/api/contacts/:id', getContact);
app.put('/api/contacts/:id', bodyParser.json(), updateContact);
app.delete('/api/contacts/:id', deleteContact);
app.post('/api/contacts/delete', bodyParser.json(), deleteContacts);

var url = 'mongodb://lecktor:qwerty@ds062059.mlab.com:62059/i399';
var dao = new Dao();

dao.connect(url)
    .then(() => app.listen(3000));

function createContact(request, response) {
    response.set('Content-Type', 'application/json');
    dao.create(request.body).then(data => {
        response.end(JSON.stringify(data));
    }).catch(error => {
        console.log(error);
        response.end('Error: ' + error);
    });
}
function getContacts(request, response) {
    response.set('Content-Type', 'application/json');
    dao.findAll().then(data => {
        response.end(JSON.stringify(data));
    }).catch(error => {
        console.log(error);
        response.end('Error: ' + error);
    });
}
function getContact(request, response) {
    var id = request.params.id;
    response.set('Content-Type', 'application/json');
    dao.findById(id).then(data => {
        response.end(JSON.stringify(data));
    }).catch(error => {
        console.log(error);
        response.end('Error: ' + error);
    });
}
function updateContact(request, response) {
    var id = request.params.id;
    response.set('Content-Type', 'application/json');
    dao.update(id, request.body).then(data => {
        response.end(JSON.stringify(data));
    }).catch(error => {
        console.log(error);
        response.end('Error: ' + error);
    });
}
function deleteContact(request, response) {
    var id = request.params.id;
    response.set('Content-Type', 'application/json');
    dao.deleteById(id).then(data => {
        response.end(JSON.stringify(data));
    }).catch(error => {
        console.log(error);
        response.end('Error: ' + error);
    });
}
function deleteContacts(request, response) {
    response.set('Content-Type', 'application/json');
    dao.deleteByIds(request.body).then(data => {
        response.end(JSON.stringify(data));
    }).catch(error => {
        console.log(error);
        response.end('Error: ' + error);
    });
}