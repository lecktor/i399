import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'searchfilter'
})

@Injectable()
export class SearchFilterPipe implements PipeTransform
{
    transform(items: any[], fields: string[], value: string): any[] {
        if (!items) return [];
        return items.filter(
            it => (
                (it[fields[0]] && it[fields[0]].toString().indexOf(value) >= 0) || (it[fields[1]] && it[fields[1]].toString().indexOf(value) >= 0)
            )
        );
    }
}