import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent }  from './app.cmp';
import { ContactService } from "./contact.srv";
import { routes } from "./routes";
import { RouterModule } from "@angular/router";
import { SearchComponent } from "./search/search.cmp";
import { EditComponent } from "./edit/edit.cmp";
import { NewComponent } from "./new/new.cmp";
import { SearchFilterPipe } from "./search.pipe";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot(routes, { useHash: true })
    ],
    declarations: [ AppComponent, SearchComponent, EditComponent, NewComponent, SearchFilterPipe ],
    providers: [ ContactService ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }