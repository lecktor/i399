import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContactService, Contact } from '../contact.srv';

@Component({
    templateUrl: 'app/new/new.html'
})
export class NewComponent implements OnInit
{
    newContactName: string;
    newContactPhone: string;

    constructor(
        private router: Router,
        private contactService: ContactService) {}

    ngOnInit(): void { }

    addNewContact(): void {
        this.contactService.saveContact(new Contact(this.newContactName, this.newContactPhone))
            .then(() => {
                this.router.navigateByUrl('/search');
            });
    }
}