import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService, Contact } from '../contact.srv';

@Component({
    templateUrl: 'app/edit/edit.html'
})

export class EditComponent implements OnInit
{
    contact: Contact;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private contactService: ContactService) {}

    ngOnInit(): void {
        const id = parseInt(this.route.snapshot.paramMap.get('id'));
        this.contactService.getContact(id)
            .then(contact => this.contact = contact);
    }

    updateContact(contact: Contact): void {
        this.contactService.updateContact(contact._id, contact)
            .then(() => {
                this.router.navigateByUrl('/search');
            });
    }
}