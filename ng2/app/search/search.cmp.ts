import { Component, OnInit } from '@angular/core';
import { Contact, ContactService } from '../contact.srv';

@Component({
    selector: 'search',
    templateUrl: 'app/search/search.html',
    styleUrls: ['app/search/search.css']
})
export class SearchComponent implements OnInit {

    contacts: Contact[] = [];
    selectedContacts: Contact[];
    newContactName: string;
    newContactPhone: string;
    searchTerm: string;

    constructor(private contactService: ContactService) {}

    private updateContacts(): void
    {
        this.contactService
            .getContacts()
            .then(contacts => this.contacts = contacts);
    }

    deleteContact(contactId: number): void
    {
        this.contactService
            .deleteContact(contactId)
            .then(() => this.updateContacts());
    }

    deleteSelectedContacts(): void
    {
        this.selectedContacts = this.contacts.filter(_ => _.selected);
        for (let contact in this.selectedContacts)
        {
            this.contactService
                .deleteContact(this.selectedContacts[contact]._id)
                .then(() => this.updateContacts());
        }
    }

    ngOnInit(): void {
        this.updateContacts();
    }

}

