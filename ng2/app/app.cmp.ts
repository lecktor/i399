import { Component } from '@angular/core';
import { Contact, ContactService } from "./contact.srv";

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.html'
})
export class AppComponent
{
    contacts: Contact[] = [];

    constructor(private contactService: ContactService) {
        this.updateContacts();
    }

    updateContacts() {
        return this.contactService.getContacts()
            .then(contacts => this.contacts = contacts);
    }
}